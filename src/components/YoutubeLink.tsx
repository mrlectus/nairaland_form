import { useForm } from 'react-hook-form';
import Input from './Input';

const YouTubeLink = () => {
  const { register, handleSubmit } = useForm();
  return (
    <form onSubmit={handleSubmit((data) => console.log(data))}>
      <div className="flex flex-col gap-3">
        <label>Enter public Youtube URL:</label>
        <Input register={register} placeholder="youtube.com/watch?v" />
        <p>Paste the link to the file you wish to upload</p>
      </div>
    </form>
  );
};

export default YouTubeLink;
