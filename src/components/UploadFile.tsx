import { useState } from 'react';
import toast from 'react-hot-toast';
import UploadButton from './UploadButton';
import { useForm } from 'react-hook-form';
import clsx from 'clsx';

const UploadFile = () => {
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm();
  const [file, setFile] = useState(null);
  const [size, setFileSize] = useState(null);
  const handleFile = (data: any) => {
    setFile(data.file[0].name);
    setFileSize(data.file[0].size);
  };
  return (
    <form
      onSubmit={handleSubmit((data) => handleFile(data))}
      className="flex w-full flex-col gap-2"
    >
      <div className="flex border-4 rounded-md items-center justify-center border-black border-dotted w-full h-48">
        <label className="flex flex-col text-center text-sm p-10 justify-center items-center w-full h-full">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0px"
            y="0px"
            width="100"
            height="100"
            viewBox="0 0 32 32"
            className="w-48 h-48"
          >
            <path
              fill="#011839"
              d="M17.13,5a1,1,0,0,0,0,2,1.64,1.64,0,0,1,1.6,1.25,1,1,0,0,0,1,.75A1.07,1.07,0,0,0,20,9a1,1,0,0,0,.72-1.22A3.65,3.65,0,0,0,17.13,5Z"
            ></path>
            <path
              fill="#011839"
              d="M27.5,11v0a4,4,0,0,0-3-3.85,9,9,0,0,0-17.06,0A4,4,0,0,0,4.5,11v0A4,4,0,0,0,5,19a1,1,0,0,0,0-2,2,2,0,0,1,0-4h.68a1,1,0,0,0,1-1.33A1.89,1.89,0,0,1,6.5,11,2,2,0,0,1,8.34,9a1,1,0,0,0,.89-.75,7,7,0,0,1,13.54,0,1,1,0,0,0,.89.75,2,2,0,0,1,1.84,2,1.89,1.89,0,0,1-.13.67A1,1,0,0,0,26.32,13H27a2,2,0,0,1,0,4,1,1,0,0,0,0,2,4,4,0,0,0,.5-8Z"
            ></path>
            <path
              fill="#7738c8"
              d="M16,13a9,9,0,1,0,9,9A9,9,0,0,0,16,13Zm2.29,8.71L17,20.41V26a1,1,0,0,1-1,1h0a1,1,0,0,1-1-1V20.41l-1.29,1.3a1,1,0,0,1-1.42,0h0a1,1,0,0,1,0-1.42l3-3a1,1,0,0,1,1.42,0l3,3a1,1,0,0,1,0,1.42h0A1,1,0,0,1,18.29,21.71Z"
            ></path>
          </svg>
          <span>drag file(s) here</span>
          <span>or, click to browse (4 MB max)</span>
          <input
            {...register('file', {
              required: true,
              validate: (value) => {
                if (value[0].size > 1024 * 1024 * 4) {
                  toast.error('exceed file max size of (4MB)');
                }
                return value[0].size <= 1024 * 1024 * 4;
              },
            })}
            type="file"
            max="4"
            className="invisible w-full h-full"
          />
        </label>
      </div>
      {file && isValid && (
        <div className=" text-xs p-1 flex flex-row items-center border rounded-md">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0px"
            y="0px"
            width="100"
            height="100"
            viewBox="0 0 30 30"
            className="w-6 h-6"
          >
            <path d="M24.707,8.793l-6.5-6.5C18.019,2.105,17.765,2,17.5,2H7C5.895,2,5,2.895,5,4v22c0,1.105,0.895,2,2,2h16c1.105,0,2-0.895,2-2 V9.5C25,9.235,24.895,8.981,24.707,8.793z M18,21h-8c-0.552,0-1-0.448-1-1c0-0.552,0.448-1,1-1h8c0.552,0,1,0.448,1,1 C19,20.552,18.552,21,18,21z M20,17H10c-0.552,0-1-0.448-1-1c0-0.552,0.448-1,1-1h10c0.552,0,1,0.448,1,1C21,16.552,20.552,17,20,17 z M18,10c-0.552,0-1-0.448-1-1V3.904L23.096,10H18z"></path>
          </svg>
          <div className="flex flex-col gap-1 min-h-10 w-full p-2 items-center justify-start">
            {isValid && (
              <span className="text-xs w-full">
                {file} {(size! / 1024 / 1024).toFixed(2)} MB
              </span>
            )}
            <div
              className={clsx(
                'bg-purple-800 h-2 w-full rounded-full',
                errors.file && 'bg-red-500'
              )}
            ></div>
          </div>
          {'100%'}
        </div>
      )}
      <div className="flex justify-end">
        <UploadButton />
      </div>
    </form>
  );
};

export default UploadFile;
