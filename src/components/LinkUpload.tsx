import { useForm } from 'react-hook-form';
import Input from './Input';

const LinkUpload = () => {
  const { register, handleSubmit } = useForm();
  return (
    <form onSubmit={handleSubmit((data) => console.log(data))}>
      <div className="flex flex-col gap-3">
        <label>Enter your file link:</label>
        <Input register={register} placeholder="example.com/" />
        <p>Paste the link to the file you wish to upload</p>
      </div>
    </form>
  );
};

export default LinkUpload;
