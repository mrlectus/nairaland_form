import React, { ButtonHTMLAttributes } from 'react';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {}
const UploadButton: React.FC<ButtonProps> = ({
  children = 'Upload',
  ...attributes
}: ButtonProps) => {
  return (
    <button
      className={`w-fit outline-none h-5 p-1 flex justify-center items-center text-white bg-purple-800 rounded-md text-sm ${attributes.className}`}
      {...attributes}
    >
      {children}
    </button>
  );
};

export default UploadButton;
