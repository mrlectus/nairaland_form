import { useForm } from 'react-hook-form';
import Input from './Input';

const GithubLink = () => {
  const { register, handleSubmit } = useForm();
  return (
    <form
      className="flex flex-col gap-2"
      onSubmit={handleSubmit((data) => console.log(data))}
    >
      <div className="flex flex-col gap-3">
        <label>Enter public Github URL:</label>
        <Input register={register} placeholder="github.com/username/repo" />
        <p>By default we collect all of the files from the repo</p>
      </div>
      <div className="flex flex-col sm:flex-row gap-2 sm:items-center justify-between">
        <label htmlFor="main">Branch</label>
        <input
          {...register('main')}
          placeholder="main"
          id="main"
          className="border border-black/70 rounded-md outline-none p-2 md:w-[70%]"
        />
      </div>
      <div className="flex flex-col sm:flex-row gap-2 sm:items-center justify-between">
        <label htmlFor="path">Folder Path</label>
        <input
          {...register('path')}
          placeholder="docs/v2"
          id="path"
          className="border border-black/70 rounded-md outline-none p-2 md:w-[70%]"
        />
      </div>
      <div className="flex flex-col sm:flex-row gap-2 sm:items-center justify-between">
        <label htmlFor="path">Include only doc files (.md/.mdx/.rst)</label>
        <select
          {...register('require')}
          id="path"
          className="border border-black/70 rounded-md outline-none p-2 md:w-[40%]"
        >
          <option>Yes</option>
          <option>No</option>
        </select>
      </div>
    </form>
  );
};

export default GithubLink;
