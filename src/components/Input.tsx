import React, { InputHTMLAttributes } from 'react';
import UploadButton from './UploadButton';
import { FieldValues, UseFormRegister } from 'react-hook-form';
interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  register: UseFormRegister<FieldValues>;
  placeholder: string;
}
const Input: React.FC<InputProps> = ({ placeholder, register, ...props }) => {
  return (
    <div className="flex items-center h-10 relative w-full">
      <input
        {...register?.('link')}
        placeholder={placeholder}
        className={`h-10 absolute outline-none border border-black/70 w-full rounded-md p-2`}
        {...props}
      />
      <UploadButton className="absolute right-0 outline-none mx-2 z-10 bg-purple-800 rounded-md h-7 text-white flex items-center p-2 text-xs" />
    </div>
  );
};
export default Input;
