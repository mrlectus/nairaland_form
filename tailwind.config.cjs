module.exports = {
  darkMode: 'class',
  content: ['./index.html','./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      backgroundImage: {},
      colors: {
        primary: {
        },
        light: {
        },
        dark: {
        },
      },
    },
    fontFamily: {
      sans: ['Space Grotesk', 'sans-serif'],
    },
  },
  plugins: [],
};
